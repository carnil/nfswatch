/*
 * $Id$
 *
 * externs.h - external definitons for nfswatch.
 *
 * David A. Curry				Jeffrey C. Mogul
 * Purdue University				Digital Equipment Corporation
 * Engineering Computer Network			Western Research Laboratory
 * 1285 Electrical Engineering Building		250 University Avenue
 * West Lafayette, IN 47907-1285		Palo Alto, CA 94301
 * davy@ecn.purdue.edu				mogul@decwrl.dec.com
 * 
 */

extern char		*pname;

extern FILE		*logfp;

extern Counter		pkt_total;
extern Counter		pkt_drops;
extern Counter		int_pkt_total;
extern Counter		int_pkt_drops;
extern Counter		dst_pkt_total;
extern Counter		int_dst_pkt_total;

extern int		errno;
extern int		bgflag;
#ifdef USE_LINUX
extern linux_socket_handle_t ls;
#else
extern int		if_fd[];
#endif
extern int		allintf;
extern int		fhdebugf;
extern int		dstflag;
extern int		srcflag;
extern int		allflag;
extern int		logging;
extern int		learnfs;
extern int		if_dlt[];
extern int		do_update;
extern int		cycletime;
extern int		totaltime;
extern int		showwhich;
extern int		serverflag;
extern unsigned int	truncation;
#ifndef USE_LINUX
extern int		ninterfaces;
#endif
extern int		sortbyusage;
extern int		nnfscounters;
extern int		nfilecounters;
extern int		nauthcounters;
extern int		screen_inited;
extern int		nclientcounters;

extern ipaddrt		thisdst;
extern ipaddrt		srcaddrs[];
extern ipaddrt		dstaddrs[];
extern ipaddrt		serveraddrs[];

extern struct timeval	starttime;

extern char		myhost[];
extern char		srchost[];
extern char		dsthost[];
extern char		serverhost[];

extern char		*prompt;
extern char		*logfile;
extern char		*mapfile;
extern char		*filelist;
extern char		*snapshotfile;

extern NFSCounter	nfs_counters[];
extern FileCounter	fil_counters[];
extern PacketCounter	pkt_counters[];
extern ProcCounter	prc_counters[NFSHIGHPROTO + 1][MAXNFS3PROC + 2];
extern int		prc_countmap[NFSHIGHPROTO + 1][MAXNFS3PROC];
extern char*            nfs_procnams[NFSHIGHPROTO + 1][MAXNFS3PROC];
extern ClientCounter	clnt_counters[];
extern AuthCounter	auth_counters[];

extern NFSCall		nfs_calls[NFSCALLHASHSIZE];

extern unsigned long	*rpcPort;
extern unsigned int	rpcPortCnt;

int			dlpi_devtype();
int			nit_devtype();
int			pfilt_devtype();
int			setup_nit_dev();
int			setup_dlpi_dev();
int			setup_pfilt_dev();
int			setup_snoop_dev();
int			snoop_devtype();

void			flush_nit();
void			flush_dlpi();
void			flush_pfilt();
void			flush_snoop();
void			setup_auth_counters();
void			get_rpc_ports(void);

#ifdef USE_LINUX
int setup_linux_dev(char **, linux_socket_handle_p_t);
int linux_read_packet(linux_socket_handle_p_t);
#endif
void ClientHashRebuild(void);
void Parse_fh(caddr_t *, my_fsid *, ino_t *, char **, char **, int);
char *map_fs_name(char *);
void snapshot(void);
void update_logfile(void);
void get_net_addrs(void);
int want_packet(ipaddrt, ipaddrt);
int to_self(ipaddrt);
#ifdef SUNOS5
void pkt_dispatch(char *, int, u_short, struct timeval *);
#endif
void pkt_filter_ether(char *, u_int, struct timeval *);
void pkt_filter_fddi(char *, u_int, struct timeval *);
void pkt_filter_sll(char *, u_int, struct timeval *);
void rpc_filter(char *, u_int, ipaddrt, ipaddrt, struct timeval *);
void setup_rpcxdr(void);
void setup_screen(char *);
void label_screen(void);
void update_screen(void);
void command(void);
char *prtime(time_t);
void error(char *);
void finish(int);
void setup_pkt_counters(void);
void setup_nfs_counters(void);
void sort_nfs_counters(void);
void setup_fil_counters(void);
void sort_prc_counters(void);
void sort_prc3_counters(void);
void setup_proc_counters(void);
void sort_clnt_counters(void);
void setup_clnt_counters(void);
void sort_auth_counters(void);
void usage(void);
void wakeup(int);
char *dlt_name(int);
void setup_map_file(void);
