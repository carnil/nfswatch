#
# $Id$
#
# Makefile for nfswatch.
#
# David A. Curry				Jeffrey C. Mogul
# Purdue University				Digital Equipment Corporation
# Engineering Computer Network			Western Research Laboratory
# 1285 Electrical Engineering Building		250 University Avenue
# West Lafayette, IN 47907-1285			Palo Alto, CA 94301
# davy@ecn.purdue.edu				mogul@decwrl.dec.com
#

#
# Choose an appropriate value for "OS" from the ones below:
#
#	IRIX40		Silicon Graphics IRIX 4.0
#	IRIX51		Silicon Graphics IRIX 5.1
#	SUNOS4		Sun Microsystems SunOS 4.x
#	SUNOS5		Sun Microsystems SunOS 5.x (Solaris 2.x)
#	SVR4		AT&T System V Release 4
#	ULTRIX		Digital Equipment Ultrix 4.x
#	DECOSF		Digital Equipment OSF/1 V1.3 and later
#
OS=

#
# Set BINDIR, MANDIR, and MANSUF to appropriate values for your system.
#
DESTDIR=
BINDIR=	/usr/sbin
MANDIR=	/usr/share/man/man8
INSTALL=install
MANSUF=	8.gz
STRIP=	-s
PKG_CONFIG?= pkg-config

IRIX40CFLAGS=	-DIRIX40 -O -cckr
IRIX51CFLAGS=	-DIRIX51 -DIRIX40 -O -cckr -D_BSD_SIGNALS
IRIX6CFLAGS=	-DIRIX6 -DIRIX51 -DIRIX40 -O -D_BSD_SIGNALS
SUNOS4CFLAGS=	-DSUNOS4 -O
SUNOS5CFLAGS=	-DSUNOS5 -O
SUNOS54CFLAGS=	-DSUNOS54 -DSUNOS5 -O
SUNOS55CFLAGS=	-DSUNOS55 -DSUNOS5 -O -Wall
SUNOS58CFLAGS=	-DSUNOS58 -DSUNOS5 -O -Wall
SVR4CFLAGS=	-DSVR4 -O
ULTRIXCFLAGS=	-DULTRIX -O
DECOSFCFLAGS=	-DDECOSF -O
LINUXCFLAGS=	-DLINUX -O -Wall -W $(shell $(PKG_CONFIG) --cflags libtirpc) $(RPM_OPT_FLAGS)

IRIX40LIBS=	-lcurses -ltermcap -lsun -lm
IRIX51LIBS=	-lcurses -ltermcap -lm
SUNOS4LIBS=	-lcurses -ltermcap -lm
SUNOS5LIBS=	-lcurses -lnsl -lsocket -lm
SUNOS54LIBS=	-lcurses -lnsl -lsocket -lm
SUNOS55LIBS=	-lcurses -lnsl -lsocket -lm
SUNOS58LIBS=	-lcurses -lnsl -lsocket -lm
SVR4LIBS=	-lcurses -lnsl -lsocket -lm
ULTRIXLIBS=	-lcurses -ltermcap -lm
#DECOSFLIBS=	-lcurses -ltermcap -lm
DECOSFLIBS=	-lcurses -ltermcap -lm pfopen.c
LINUXLIBS=	-lpcap $(shell $(PKG_CONFIG) --libs libtirpc) -lncurses -lm

CFLAGS=
LIBS=

PRINT=	enscript -r -G
SHELL=	/bin/sh

HDRS=	externs.h nfswatch.h rpcdefs.h screen.h
SRCS=	dlpi.c logfile.c netaddr.c nfswatch.c nit.c pfilt.c pktfilter.c \
	rpcfilter.c rpcutil.c screen.c snoop.c util.c xdr.c nfslogsum.c \
	parsenfsfh.c linux.c
OBJS=	dlpi.o logfile.o netaddr.o nfswatch.o nit.o pfilt.o pktfilter.o \
	rpcfilter.o rpcutil.o screen.o snoop.o util.o xdr.o parsenfsfh.o \
	linux.o

all: os-nfswatch os-nfslogsum

os-nfswatch:
	@if [ "$(OS)" = "" ]; then \
	    OS=`uname -s -r`; \
	fi; \
	case "$$OS" in \
	LINUX\ *|Linux\ *) \
	    make CFLAGS="$(LINUXCFLAGS)" LIBS="$(LINUXLIBS)" nfswatch; \
	    ;; \
	IRIX40|IRIX\ 4*) \
	    make CFLAGS="$(IRIX40CFLAGS)" LIBS="$(IRIX40LIBS)" nfswatch; \
	    ;; \
	IRIX51|IRIX\ 5*) \
	    make CFLAGS="$(IRIX51CFLAGS)" LIBS="$(IRIX51LIBS)" nfswatch; \
	    ;; \
	IRIX6|IRIX\ 6*) \
	    make CC=gcc CFLAGS="$(IRIX6CFLAGS)" LIBS="$(IRIX51LIBS)" nfswatch; \
	    ;; \
	SUNOS4|SunOS\ 4*) \
	    make CFLAGS="$(SUNOS4CFLAGS)" LIBS="$(SUNOS4LIBS)" nfswatch; \
	    ;; \
	SUNOS5|SunOS\ 5.[0123]) \
	    make CFLAGS="$(SUNOS5CFLAGS)" LIBS="$(SUNOS5LIBS)" nfswatch; \
	    ;; \
	SUNOS54|SunOS\ 5.4) \
	    make CFLAGS="$(SUNOS54CFLAGS)" LIBS="$(SUNOS54LIBS)" nfswatch; \
	    ;; \
	SUNOS55|SunOS\ 5.[567]) \
	    make CFLAGS="$(SUNOS55CFLAGS)" LIBS="$(SUNOS55LIBS)" nfswatch; \
	    ;; \
	SUNOS58|SunOS\ 5.*) \
	    make CFLAGS="$(SUNOS58CFLAGS)" LIBS="$(SUNOS58LIBS)" nfswatch; \
	    ;; \
	SVR4|System\ V\ Release\ 4*) \
	    make CFLAGS="$(SVR4CFLAGS)" LIBS="$(SVR4LIBS)" nfswatch; \
	    ;; \
	ULTRIX\ *|Ultrix\ *|ultrix\ *) \
	    make CFLAGS="$(ULTRIXCFLAGS)" LIBS="$(ULTRIXLIBS)" nfswatch; \
	    ;; \
	OSF1\ *|OSF\ *) \
	    make CFLAGS="$(DECOSFCFLAGS)" LIBS="$(DECOSFLIBS)" nfswatch; \
	    ;; \
	*) \
	    echo "OS=$$OS is not a supported operating system."; \
	    exit 1; \
	    ;; \
	esac

os-nfslogsum:
	@if [ "$(OS)" = "" ]; then \
	    OS=`uname -s -r`; \
	fi; \
	case "$$OS" in \
	LINUX|Linux\ *) \
	    make CFLAGS="$(LINUXCFLAGS)" LIBS="$(LINUXLIBS)" nfslogsum; \
	    ;; \
	IRIX40|IRIX\ 4*) \
	    make CFLAGS="$(IRIX40CFLAGS)" LIBS="$(IRIX40LIBS)" nfslogsum; \
	    ;; \
	IRIX51|IRIX\ 5*) \
	    make CFLAGS="$(IRIX51CFLAGS)" LIBS="$(IRIX51LIBS)" nfslogsum; \
	    ;; \
	IRIX6|IRIX\ 6*) \
	    make CC=gcc CFLAGS="$(IRIX6CFLAGS)" LIBS="$(IRIX51LIBS)" nfslogsum; \
	    ;; \
	SUNOS4|SunOS\ 4*) \
	    make CFLAGS="$(SUNOS4CFLAGS)" LIBS="$(SUNOS4LIBS)" nfslogsum; \
	    ;; \
	SUNOS5|SunOS\ 5*) \
	    make CFLAGS="$(SUNOS5CFLAGS)" LIBS="$(SUNOS5LIBS)" nfslogsum; \
	    ;; \
	SVR4|System\ V\ Release\ 4*) \
	    make CFLAGS="$(SVR4CFLAGS)" LIBS="$(SVR4LIBS)" nfslogsum; \
	    ;; \
	ULTRIX\ *|Ultrix\ *|ultrix\ *) \
	    make CFLAGS="$(ULTRIXCFLAGS)" LIBS="$(ULTRIXLIBS)" nfslogsum; \
	    ;; \
	OSF1\ *|OSF\ *) \
	    make CFLAGS="$(DECOSFCFLAGS)" LIBS="$(DECOSFLIBS)" nfslogsum; \
	    ;; \
	*) \
	    echo "OS=$$OS is not a supported operating system."; \
	    exit 1; \
	    ;; \
	esac

nfswatch: nfswatch.o $(OBJS)
	$(CC) -o nfswatch $(OBJS) $(LIBS)

nfslogsum: nfslogsum.o
	$(CC) -o nfslogsum nfslogsum.o

install: bininstall maninstall

bininstall: all
	mkdir -p $(DESTDIR)$(BINDIR)
	$(INSTALL) -c $(STRIP) nfswatch $(DESTDIR)$(BINDIR)
	$(INSTALL) -c $(STRIP) nfslogsum $(DESTDIR)$(BINDIR)

maninstall: nfswatch-man nfslogsum-man

nfswatch-man: $(DESTDIR)$(MANDIR)/nfswatch.$(MANSUF)

$(DESTDIR)$(MANDIR)/nfswatch.$(MANSUF): nfswatch.8
	mkdir -p $(DESTDIR)$(MANDIR)
	case "$(MANSUF)" in \
	*.gz) \
	    SUF=`echo $(MANSUF)|sed 's/\.gz//'`; \
	    $(INSTALL) -c -m 0644 nfswatch.8 $(DESTDIR)$(MANDIR)/nfswatch.$$SUF; \
	    gzip --best $(DESTDIR)$(MANDIR)/nfswatch.$$SUF; \
	    ;; \
	*) \
	    $(INSTALL) -c -m 0644 nfswatch.8 $(DESTDIR)$(MANDIR)/nfswatch.$(MANSUF); \
	    ;; \
	esac

nfslogsum-man: $(DESTDIR)$(MANDIR)/nfslogsum.$(MANSUF)

$(DESTDIR)$(MANDIR)/nfslogsum.$(MANSUF): nfslogsum.8
	mkdir -p $(DESTDIR)$(MANDIR)
	case "$(MANSUF)" in \
	*.gz) \
	    SUF=`echo $(MANSUF)|sed 's/\.gz//'`; \
	    $(INSTALL) -c -m 0644 nfslogsum.8 $(DESTDIR)$(MANDIR)/nfslogsum.$$SUF; \
	    gzip --best $(DESTDIR)$(MANDIR)/nfslogsum.$$SUF; \
	    ;; \
	*) \
	    $(INSTALL) -c -m 0644 nfslogsum.8 $(DESTDIR)$(MANDIR)/nfslogsum.$(MANSUF); \
	    ;; \
	esac

print:
	$(PRINT) Makefile $(HDRS) $(SRCS)

clean:
	rm -f \#* a.out core nfswatch nfslogsum nfslogsum.o $(OBJS)
	rm -f *.BAK *.CKP

dlpi.o:		dlpi.c nfswatch.h externs.h os.h nfsfh.h
logfile.o:	logfile.c nfswatch.h externs.h screen.h os.h nfsfh.h
netaddr.o:	netaddr.c nfswatch.h externs.h os.h nfsfh.h
nfslogsum.o:	nfslogsum.c nfswatch.h os.h nfsfh.h
nfswatch.o:	nfswatch.c nfswatch.h os.h nfsfh.h
nit.o:		nit.c nfswatch.h externs.h os.h nfsfh.h
pfilt.o:	pfilt.c nfswatch.h externs.h os.h nfsfh.h
pktfilter.o:	pktfilter.c nfswatch.h externs.h os.h nfsfh.h
rpcfilter.o:	rpcfilter.c nfswatch.h externs.h rpcdefs.h os.h nfsfh.h
rpcutil.o:	rpcutil.c nfswatch.h externs.h rpcdefs.h os.h nfsfh.h
screen.o:	screen.c nfswatch.h externs.h screen.h os.h nfsfh.h
snoop.o:	snoop.c nfswatch.h externs.h os.h nfsfh.h
linux.o:	linux.c nfswatch.h externs.h os.h nfsfh.h
util.o:		util.c nfswatch.h externs.h screen.h os.h nfsfh.h
xdr.o:		xdr.c nfswatch.h os.h nfsfh.h
parsenfsfh.o:	parsenfsfh.c nfsfh.h
