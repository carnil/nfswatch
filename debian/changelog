nfswatch (4.99.14-1) unstable; urgency=medium

  * New upstream version 4.99.14
  * Declare compliance with Debian policy 4.7.0
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 01 Feb 2025 16:59:24 +0100

nfswatch (4.99.13-1) unstable; urgency=medium

  * New upstream version 4.99.13
    - Fixes FTCBFS: Do not hard-code the build architecture pkg-config
      (Closes: #1067119)
  * Drop patches applied upstream
  * Declare compliance with Debian policy 4.6.2
  * Update copyright years for debian/* packaging files
  * Replace Build-Depends on pkg-config with pkgconf
  * Replace Build-Depends on libncurses5-dev with libncurses-dev

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 21 Mar 2024 14:08:38 +0100

nfswatch (4.99.12-1) unstable; urgency=medium

  * New upstream version 4.99.12
    - Drop patches applied upstream
  * Declare compliance with Debian policy 4.6.1
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 05 Aug 2022 08:00:27 +0200

nfswatch (4.99.11-9) unstable; urgency=medium

  * Use mvaddstr() instead of mvprintw() for dynamic string (Closes: #997185)
  * Rename Salsa CI configuration file to debian/salsa-ci.yml
  * Declare compliance with Debian policy 4.6.0
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 13 Nov 2021 10:58:26 +0100

nfswatch (4.99.11-8) unstable; urgency=medium

  * Fix typo causing nfswatch to still use partially the glibc SunRPC
    implementation.  Thanks to Aurelien Jarno (Closes: #968732)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 22 Aug 2020 17:30:57 +0200

nfswatch (4.99.11-7) unstable; urgency=medium

  * Update .gitlab-ci.yml based on Salsa CI Team's template
  * Switch from glibc SunRPC libary to libtirpc.
    Add Build-Depends on libtirpc-dev and pkg-config and pass include
    directory for libtirpc headers on build and link libtirpc library.
    Thanks to Ben Hutchings (Closes: #954380)
  * debian/control: Wrap and sort fields
  * debian/watch: Bump format to version 4.
    Use @PACKAGE@, @ANY_VERSION@ and @ARCHIVE_EXT@ substitutions.
  * Update copyright years for debian/* packaging files
  * Declare compliance with Debian policy 4.5.0
  * Bump Debhelper compat level to 13

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 18 Apr 2020 23:33:26 +0200

nfswatch (4.99.11-6) unstable; urgency=medium

  * Replace home-made GitLab CI with the standard Salsa pipeline
  * Inject additional CFLAGS from dpkg-buildflags into build
  * Bump Debhelper compat level to 12
  * Declare compliance with Debian policy 4.3.0

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 28 Dec 2018 21:38:51 +0100

nfswatch (4.99.11-5) unstable; urgency=medium

  * debian/changelog: Remove trailing whitespaces
    bp-Dch: Ignore
  * Add an initial Gitlab CI config file
  * debian/.gitlab-ci.yml: Update image used for Gitlab CI
  * Update .gitlab-ci.yml based on Salsa CI Team's template
  * GitLab CI/Lintian: install dpkg-dev, that ships dpkg-architecture, needed
    to run some Lintian checks
  * Fix build failure ('major'/'minor' undefined) in glibc 2.28.
    Thanks to Adrian Bunk <bunk@debian.org> (Closes: #916003)
  * Declare compliance with Debian policy 4.2.1

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 09 Dec 2018 14:40:06 +0100

nfswatch (4.99.11-4) unstable; urgency=medium

  * Add Vcs-* fields for packaging hosting on salsa.debian.org
  * Update copyright years for debian/* packaging files
  * Bump Debhelper compat level to 11
  * Use HTTPS transport protocol for specification URL of copyright format
  * Declare compliance with Debian policy 4.1.3
  * debian/copyright: Clarify license short-names.
    Use BSD-3-clause short name and remove ambiguous BSD keyword.
    Move BSD-3-clause license to own standalone stanza.
  * Set Rules-Requires-Root to no

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 19 Mar 2018 16:20:26 +0100

nfswatch (4.99.11-3) unstable; urgency=low

  * Add fix-typo-in-error-message.patch patch.
    Fixes typo in error message "nfswatch: must specify file list with -f"
    instead of "nfswatch: must specify file list with -fi."
  * Update copyright years for debian/* packaging files
  * Bump Standards-Version to 3.9.4

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 26 Sep 2013 20:51:44 +0200

nfswatch (4.99.11-2) unstable; urgency=low

  * Email change: Salvatore Bonaccorso -> carnil@debian.org
  * debian/control: Drop the DM-Upload-Allowed control field.
  * Bump Debhelper compat level to 9.
    Adjust Build-Depends for debehlper to (>= 9).
  * Update debian/copyright file.
    Update copyright years for debian/* packaging.
    Update format to copyright-format 1.0 as released together with Debian
    policy 3.9.3.
  * Bump Standards-Version to 3.9.3

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 30 Mar 2012 07:19:17 +0200

nfswatch (4.99.11-1) unstable; urgency=low

  * New upstream version
  * Drop 02-545853-Fix-FTBFS-on-alpha.patch patch as it is applied upstream.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Sun, 25 Apr 2010 14:15:53 +0200

nfswatch (4.99.10-5) unstable; urgency=low

  * Convert to '3.0 (quilt)' package source format.
    - Remove README.source file, as not needed with '3.0 (quilt) format.
    - Drop use of quilt in debian/rules and in Build-Depends in control file.
  * Bump Standards-Version to 3.8.4 (no changes).
  * debian/copyright: Referesh Format-Specification URL and update Copyright
    years for debian/* packaging.
  * Refresh 02-545853-Fix-FTBFS-on-alpha.patch with proposed patch by
    upstream.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Thu, 22 Apr 2010 13:31:44 +0200

nfswatch (4.99.10-4) unstable; urgency=low

  * Add 02-545853-Fix-FTBFS-on-alpha.patch patch to not include nfs/nfs.h
    in rpcfilter.c. Fixes FTBFS on alpha architecture (Closes: 545853).
    Thanks to Karl Goetz for giving access to an alpha machine.
  * debian/control: Expand the to short long-description about what nfswatch
    can monitor.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Fri, 11 Sep 2009 08:29:07 +0200

nfswatch (4.99.10-3) unstable; urgency=low

  * New Maintainer (Closes: #543937).
  * Add debian/watch file to source package.
  * Add README.source to document the patch system which is used. This
    is recommended since Debian Policy 3.8.0.
  * Refresh header of patch 01-manpage.patch
  * debian/copyright:
    - Update debian/* packaging section adding myself to Copyright
    - Update file pseudo-headers for Format-Specification in DEP5.
  * Minimize debian/rules using override targets and tiny rules file.
    Adjust Build-Depends on debhelper (>= 7.0.50) and quilt (>= 0.46-7)
    for required versions.
  * Add DM-Upload-Allowed: yes field.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Sat, 05 Sep 2009 12:13:10 +0200

nfswatch (4.99.10-2) unstable; urgency=low

  * Removing vcs fields.
  * Orphaning package.

 -- Daniel Baumann <daniel@debian.org>  Thu, 27 Aug 2009 16:42:39 +0200

nfswatch (4.99.10-1) unstable; urgency=low

  * Merging upstream version 4.99.10.
  * Adding build-depends to libpcap0.8-dev.

 -- Daniel Baumann <daniel@debian.org>  Fri, 17 Apr 2009 21:24:00 +0200

nfswatch (4.99.9-2) unstable; urgency=low

  * Updating package to standards 3.8.1.
  * Prefixing debhelper files with package name.
  * Removing useless dh_link call in rules.

 -- Daniel Baumann <daniel@debian.org>  Sun, 22 Mar 2009 16:21:00 +0100

nfswatch (4.99.9-1) unstable; urgency=low

  * Initial release (Closes: #512789).

 -- Daniel Baumann <daniel@debian.org>  Sat, 24 Jan 2009 03:51:00 +0100
