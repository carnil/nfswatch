/*
 * $Id$
 *
 * os.h	- operating system definitions.
 *
 * David A. Curry				Jeffrey C. Mogul
 * Purdue University				Digital Equipment Corporation
 * Engineering Computer Network			Western Research Laboratory
 * 1285 Electrical Engineering Building		250 University Avenue
 * West Lafayette, IN 47907-1285		Palo Alto, CA 94301
 * davy@ecn.purdue.edu				mogul@decwrl.dec.com
 *
 */
#ifdef LINUX
#ifndef USE_LINUX
#define USE_LINUX	1
#endif
#ifndef _GNU_SOURCE
#define _GNU_SOURCE	1
#endif
#endif

#ifdef IRIX40
#ifndef USE_SNOOP
#define USE_SNOOP	1
#endif
#define signal		sigset
#define	U_INT32_DECLARED_IN_AUTH	1
#endif

#ifdef SUNOS4
#ifndef USE_NIT
#define USE_NIT	1
#endif
#define	U_INT32_DECLARED_IN_AUTH	1
#endif

#ifdef SUNOS5
#ifndef SVR4
#define SVR4		1
#endif
#ifndef USE_DLPI
#define USE_DLPI	1
#endif
#define	U_INT32_DECLARED_IN_AUTH	1
#endif

#if defined(SUNOS55) || defined(SUNOS58)
#ifndef SUNOS54
#define SUNOS54		1
#endif
#undef U_INT32_DECLARED_IN_AUTH
#endif

#ifdef SVR4
#ifndef USE_DLPI
#define USE_DLPI	1
#endif
#define index		strchr
#define rindex		strrchr
#define signal		sigset
#define bzero(b,n)	memset(b,0,n)
#define bcmp(a,b,n)	memcmp(a,b,n)
#define bcopy(a,b,n)	memcpy(b,a,n)
#endif

#ifdef ULTRIX
#ifndef USE_PFILT
#define USE_PFILT	1
#endif
#endif

#ifdef DECOSF
#ifndef USE_PFILT
#define USE_PFILT	1
#endif
#endif
