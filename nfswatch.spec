Summary: An NFS traffic monitoring tool
Name: nfswatch
Version: 4.99.14
Release: 1%{?dist}

License: BSD
URL: http://nfswatch.sourceforge.net
Source0: http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz

BuildRequires: make
BuildRequires: gcc libpcap-devel ncurses-devel libtirpc-devel

%description
Nfswatch is a command-line tool for monitoring NFS traffic.
Nfswatch can capture and analyze the NFS packets on a particular
network interface or on all interfaces.

Install nfswatch if you need a program to monitor NFS traffic.

%prep
%setup -q

%build
make

%install
rm -rf ${RPM_BUILD_ROOT}
make STRIP= MANSUF=8 DESTDIR=${RPM_BUILD_ROOT} install

%files
%doc	LICENSE README
%{_sbindir}/nfswatch
%{_sbindir}/nfslogsum
%{_mandir}/man8/*

%changelog
* Thu Jan 30 2025 Christian Iseli <christian.iseli@epfl.ch> 4.99.14-1
 - Fix FTBFS in Fedora 42 with GCC version 15
   due to problems with old-style function declarations

* Tue Mar 20 2024 Christian Iseli <christian.iseli@epfl.ch> 4.99.13-1
 - Warn only once when receiving packets for an unsupported NFS version
 - nfswatch fails to cross build from source, because the upstream
   Makefile hard codes the build architecture pkg-config. After making
   it substitutable, it cross builds just fine as dh_auto_build passes
   the standard substitution.
 - Fix typo in error mesage when filelist is missing
 - Correct manpage sections.
 - rm ChangeLog now that we are in git; note one more change in spec
   and README

* Tue Aug  2 2022 Christian Iseli <Christian.Iseli@epfl.ch> 4.99.12-1
 - use libtirpc
 - replace sighold() and sigrelse() by sigprocmask()
 - include <sys/sysmacros.h> in util.c
 - fix printf format issues in screen.c
 - fix prototype of rpcxdr_getargs() in rpcutil.c

* Fri Apr 23 2010 Christian Iseli <Christian.Iseli@licr.org> 4.99.11-1
 - 2010-04-22 17:56  c4chris
	* rpcfilter.c: Fix build on alpha Linux.
 - 2010-04-21 17:21  c4chris
	* ChangeLog: Add ChangeLog file.
 - 2009-04-21 07:42  c4chris
	* nfswatch.c, pktfilter.c: Fix compile on non-Linux (patch from
	  Helen Chao).
 - 2009-04-15 18:26  c4chris
	* README: Convert to utf8.
 - 2009-04-15 18:21  c4chris
	* netaddr.c: Add TODO item about broken strict aliasing rule.
 - 2009-04-15 17:43  c4chris
	* nfswatch.spec: Update changelog.
* Wed Apr 15 2009 Christian Iseli <Christian.Iseli@licr.org> 4.99.10-1
 - 2009-04-15 17:40  c4chris
	* README, nfswatch.spec: Bump version number to 4.99.10.
 - 2009-02-18 13:24  c4chris
	* nfswatch.c: Add TODO item, count by interface.
 - 2009-02-03 13:51  c4chris
	* externs.h, linux.c, nfswatch.c, pktfilter.c: Introduce
	  pkt_filter_sll() and use it.
 - 2009-02-02 20:14  c4chris
	* nfswatch.c: Print friendlier message.
 - 2009-02-02 20:10  c4chris
	* linux.c: Attempt to properly grok LINUX_SLL packets.
 - 2009-02-02 00:33  c4chris
	* Makefile, externs.h, linux.c, nfswatch.c, nfswatch.h, util.c: Use
	  the pcap library for Linux.  Still need to grok the LINUX_SLL
	  link type...
 - 2008-12-11 15:46  c4chris
	* netaddr.c, rpcfilter.c: Add TODO items.
 - 2007-05-25 16:41  c4chris
	* nfswatch.spec: Update changelog.
* Fri May 25 2007 Christian Iseli <Christian.Iseli@licr.org> 4.99.9-1
 - 2007-05-25 16:40  c4chris
	* README, nfswatch.h, nfswatch.spec: Bump version number to 4.99.9.
 - 2007-05-18 18:03  c4chris
	* rpcfilter.c: Do not handle the second argument of RENAME3 and
	  LINK3, as it doesn't seem to work anyway.
 - 2007-03-28 23:50  c4chris
	* parsenfsfh.c: Add missing include to use ntohl().
 - 2007-03-28 23:43  c4chris
	* parsenfsfh.c, rpcfilter.c, util.c: Improve filehandle decoding on
	  Linux.
 - 2007-03-28 17:39  c4chris
	* util.c: Exclude known non-exports instead of guessing exports.
 - 2007-03-28 16:45  c4chris
	* rpcfilter.c: Handle more Linux filehandle's fsid_type.
 - 2007-03-13 14:27  c4chris
	* nfswatch.spec: Update changelog.

* Tue Mar 13 2007 Christian Iseli <Christian.Iseli@licr.org> 4.99.8-1
 - 2007-03-13 14:25  c4chris
	* nfswatch.h, nfswatch.spec: Bump version number to 4.99.8.
 - 2007-03-13 14:18  c4chris
	* README: Bring README file up to date.
 - 2007-03-12 19:32  c4chris
	* dlpi.c: More careful parsing of device name and instance number.
 - 2007-03-12 18:03  c4chris
	* parsenfsfh.c: Handle more Linux filehandle's fsid_type.
 - 2007-01-30 15:59  c4chris
	* nfswatch.spec: Update changelog.

* Tue Jan 30 2007 Christian Iseli <Christian.Iseli@licr.org> 4.99.7-1
 - 2007-01-30 15:47  c4chris
	* nfswatch.h, nfswatch.spec: Bump version number to 4.99.7.
 - 2007-01-30 15:44  c4chris
	* nfswatch.spec: Use updated install target of Makefile.
 - 2007-01-30 15:41  c4chris
	* Makefile: Put in system binaries.
 - 2007-01-30 15:37  c4chris
	* Makefile: Render Makefile a bit more Linux friendly.
 - 2006-12-20 16:15  c4chris
	* Makefile, externs.h, logfile.c, nfswatch.c, nfswatch.h,
	  rpcfilter.c, screen.c, util.c: Add Nick Williams's patch to make
	  the per-procedure statistics work for NFSv3.
 - 2006-06-14 13:30  c4chris
	* nfswatch.spec: Update changelog.

* Wed Jun 14 2006 Christian Iseli <Christian.Iseli@licr.org> 4.99.6-1
 - 2006-06-14 13:21  c4chris
	* nfswatch.h, nfswatch.spec: Bump version number to 4.99.6.
 - 2006-06-14 12:50  c4chris
	* Makefile, rpcdefs.h, rpcfilter.c, rpcutil.c, sgi.map.h, xdr.c:
	  Fix to get things to compile on IRIX6.
 - 2006-06-13 15:22  c4chris
	* rpcutil.c: Solaris has one additional field in struct xp_ops.
 - 2006-06-13 15:14  c4chris
	* dlpi.c: The alarm call cannot return an error.
 - 2006-06-13 15:12  c4chris
	* dlpi.c: Remove unused parameter.
 - 2006-06-13 15:10  c4chris
	* dlpi.c: Fix signed vs unsigned comparison warnings.
 - 2006-06-13 11:16  c4chris
	* Makefile, externs.h, nfswatch.c, pktfilter.c, rpcfilter.c,
	  util.c: Cleanup unused parameters.
 - 2006-06-13 10:47  c4chris
	* netaddr.c, pktfilter.c: Fix signed vs unsigned comparison
	  warning.
 - 2006-06-13 10:41  c4chris
	* nfswatch.h, rpcfilter.c: Fix way to handle long usernames.
 - 2006-06-12 23:12  c4chris
	* rpcutil.c: Avoid warning about type-punned pointer.
 - 2005-12-06 13:09  c4chris
	* rpcfilter.c: Special case empty filehandle in LINK3.
 - 2005-12-05 15:34  c4chris
	* rpcfilter.c: Special case empty filehandle in RENAME3.
 - 2005-11-29 21:45  c4chris
	* screen.c: Fix 32bit-ism.
 - 2005-11-22 23:11  c4chris
	* nfswatch.spec: Update changelog.

* Tue Nov 22 2005 Christian Iseli <Christian.Iseli@licr.org> 4.99.5-1
 - 2005-11-21 19:07  c4chris
	* nfswatch.h, nfswatch.spec: Bump version number to 4.99.5.
 - 2005-11-21 17:54  c4chris
	* Makefile, os.h, rpcdefs.h, rpcfilter.c, xdr.c: Allow to build on
	  older (5.7 and below) Solaris.
 - 2005-11-21 15:41  c4chris
	* dlpi.c, nfswatch.c: Allow 64-bit compiles on Solaris.
 - 2005-11-21 14:46  c4chris
	* externs.h, linux.c, nfswatch.c, os.h, pktfilter.c, rpcfilter.c,
	  screen.c, xdr.c: Cleanup some GCC warning messages on Linux.
 - 2005-11-16 16:01  c4chris
	* pktfilter.c: Attempt to grab NFS over TCP.
 - 2005-07-19 16:53  c4chris
	* logfile.c: Enable screendumps in ncurses.
 - 2005-07-19 15:27  c4chris
	* rpcfilter.c: Print more debug info when length is strange.
 - 2005-07-19 15:10  c4chris
	* nfslogsum.c, nfswatch.h, rpcfilter.c, util.c: Display portmap
	  instead of NIS.
 - 2005-07-19 15:03  c4chris
	* externs.h, nfswatch.c, pktfilter.c, rpcdefs.h, rpcfilter.c: Add
	  and use call to portmapper.
 - 2005-07-19 14:59  c4chris
	* screen.c, screen.h, util.c: Provide wider interval column.
 - 2005-07-12 23:39  c4chris
	* nfswatch.spec: Update changelog.

* Tue Jul 12 2005 Christian Iseli <Christian.Iseli@licr.org> 4.99.4-1
 - 2005-07-12 23:28  c4chris
	* nfswatch.h, nfswatch.spec: Bump version number to 4.99.4.
 - 2005-07-12 23:25  c4chris
	* parsenfsfh.c: Handle linux in Parse_fh().
 - 2005-07-12 23:24  c4chris
	* screen.c: Fix total NFS packets count.
 - 2005-06-02 15:36  c4chris
	* nfswatch.spec: Update changelog.

* Thu Jun  2 2005 Christian Iseli <Christian.Iseli@licr.org> 4.99.3-1
 - 2005-06-02 15:32  c4chris
	* nfswatch.h, nfswatch.spec: Bump version number to 4.99.3.
 - 2005-06-02 15:22  c4chris
	* util.c: Add xfs to the list of recognized filesystems.
 - 2005-04-22 17:53  c4chris
	* nfswatch.spec: Update changelog.

* Fri Apr 22 2005 Christian Iseli <Christian.Iseli@licr.org> 4.99.2-1
 - 2005-04-22 17:50  c4chris
	* nfswatch.h, nfswatch.spec: Bump version number to 4.99.2.
 - 2005-04-22 17:46  c4chris
	* nfswatch.spec: Fix spec file for Fedora Extras.
 - 2005-04-22 17:30  c4chris
	* Makefile: Handle RPM_OPT_FLAGS in Makefile.
 - 2005-02-25 22:54  c4chris
	* rpcfilter.c, rpcutil.c, xdr.c: Add a few comments.
 - 2005-02-25 17:49  c4chris
	* nfswatch.spec: Update changelog.

* Fri Feb 25 2005 Christian Iseli <Christian.Iseli@licr.org> 4.99.1-1
 - 2005-02-25 17:43  c4chris
	* nfswatch.h, nfswatch.spec: Bump version number to 4.99.1.
 - 2005-02-25 17:34  c4chris
	* nfswatch.h, util.c: Fix c_proc size in a couple structures.
 - 2005-02-25 16:14  c4chris
	* nfswatch.8: Add Linux info to man page.
 - 2005-02-25 15:09  c4chris
	* rpcutil.c: Fix xdr_callmsg usage on 64-bit Linux.
 - 2005-02-25 10:52  c4chris
	* linux.map.h, netaddr.c, parsenfsfh.c, rpcfilter.c, util.c: Parse
	  NFS file handles in Linux.
 - 2005-02-23 23:50  c4chris
	* linux.map.h, nfswatch.c, nfswatch.h, rpcdefs.h, rpcfilter.c,
	  screen.c, util.c: Get NFS3 to compile on Linux.  Cleanup a bit.
 - 2005-02-23 22:41  c4chris
	* Makefile, externs.h, rpcdefs.h, rpcfilter.c, xdr.c: Finish NFS3
	  setup for Solaris.
 - 2005-02-23 16:11  c4chris
	* externs.h, nfswatch.c, nfswatch.h, rpcdefs.h, rpcfilter.c,
	  screen.c, util.c, xdr.c: Started crude hack to get NFS3 working
	  on Solaris.
 - 2005-02-07 19:21  c4chris
	* linux.map.h, nfslogsum.c, nfswatch.h, osf.map.h, pktfilter.c,
	  rpcfilter.c, sgi.map.h, ultrix.map.h, util.c: Start work on
	  NFSv3.  Remove ND (Sun's network disk) stuff.

* Sun Feb  6 2005 Christian Iseli <Christian.Iseli@licr.org> 4.99.0-1
 - Create spec file.
