/*
 * $Id$
 *
 * rpcdefs.h - definitions for RPC processing code.
 *
 * David A. Curry				Jeffrey C. Mogul
 * Purdue University				Digital Equipment Corporation
 * Engineering Computer Network			Western Research Laboratory
 * 1285 Electrical Engineering Building		250 University Avenue
 * West Lafayette, IN 47907-1285		Palo Alto, CA 94301
 * davy@ecn.purdue.edu				mogul@decwrl.dec.com
 * 
 */

#define NFS_READ		0
#define NFS_WRITE		1

/*
 * RPC programs, from "Remote Procedure Call Programming Guide",
 * Revision A, 9 May 1988, pp. 64-65.
 */
#define RPC_PMAPPROG		((uint32_t) 100000) /* portmapper		     */
#define RPC_RSTATPROG		((uint32_t) 100001) /* remote stats	     */
#define RPC_RUSERSPROG		((uint32_t) 100002) /* remote users	     */
#define RPC_NFSPROG		((uint32_t) 100003) /* NFS		     */
#define RPC_YPPROG		((uint32_t) 100004) /* Yellow Pages	     */
#define RPC_MOUNTPROG		((uint32_t) 100005) /* mount daemon	     */
#define RPC_DBXPROG		((uint32_t) 100006) /* remote dbx		     */
#define RPC_YPBINDPROG		((uint32_t) 100007) /* yp binder		     */
#define RPC_WALLPROG		((uint32_t) 100008) /* shutdown msg	     */
#define RPC_YPPASSWDPROG	((uint32_t) 100009) /* yppasswd server	     */
#define RPC_ETHERSTATPROG	((uint32_t) 100010) /* ether stats	     */
#define RPC_RQUOTAPROG		((uint32_t) 100011) /* disk quotas	     */
#define RPC_SPRAYPROG		((uint32_t) 100012) /* spray packets	     */
#define RPC_IBM3270PROG		((uint32_t) 100013) /* 3270 mapper	     */
#define RPC_IBMRJEPROG		((uint32_t) 100014) /* RJE mapper	     */
#define RPC_SELNSVCPROG		((uint32_t) 100015) /* selection service     */
#define RPC_RDATABASEPROG	((uint32_t) 100016) /* remote database access */
#define RPC_REXECPROG		((uint32_t) 100017) /* remote execution	     */
#define RPC_ALICEPROG		((uint32_t) 100018) /* Alice Office Automation */
#define RPC_SCHEDPROG		((uint32_t) 100019) /* scheduling service     */
#define RPC_LOCKPROG		((uint32_t) 100020) /* local lock manager     */
#define RPC_NETLOCKPROG		((uint32_t) 100021) /* network lock manager   */
#define RPC_X25PROG		((uint32_t) 100022) /* X.25 inr protocol     */
#define RPC_STATMON1PROG	((uint32_t) 100023) /* status monitor 1	     */
#define RPC_STATMON2PROG	((uint32_t) 100024) /* status monitor 2	     */
#define RPC_SELNLIBPROG		((uint32_t) 100025) /* selection library     */
#define RPC_BOOTPARAMPROG	((uint32_t) 100026) /* boot parameters service */
#define RPC_MAZEPROG		((uint32_t) 100027) /* mazewars game     */
#define RPC_YPUPDATEPROG	((uint32_t) 100028) /* yp update	     */
#define RPC_KEYSERVEPROG	((uint32_t) 100029) /* key server	     */
#define RPC_SECURECMDPROG	((uint32_t) 100030) /* secure login	     */
#define RPC_NETFWDIPROG		((uint32_t) 100031) /* NFS net forwarder init */
#define RPC_NETFWDTPROG		((uint32_t) 100032) /* NFS net forwarder trans */
#define RPC_SUNLINKMAP_PROG	((uint32_t) 100033) /* sunlink MAP	     */
#define RPC_NETMONPROG		((uint32_t) 100034) /* network monitor	     */
#define RPC_DBASEPROG		((uint32_t) 100035) /* lightweight database  */
#define RPC_PWDAUTHPROG		((uint32_t) 100036) /* password authorization */
#define RPC_TFSPROG		((uint32_t) 100037) /* translucent file svc   */
#define RPC_NSEPROG		((uint32_t) 100038) /* nse server	     */
#define RPC_NSE_ACTIVATE_PROG	((uint32_t) 100039) /* nse activate daemon    */
#define RPC_NIS			((uint32_t) 100300) /* nis+ daemon	     */
#define RPC_CACHEPROG		((uint32_t) 100301) /* nis+ cache	     */
#define RPC_CB_PROG		((uint32_t) 100302) /* nis+ callback	     */

#define RPC_PCNFSDPROG		((uint32_t) 150001) /* pc passwd authorization */

#define RPC_PYRAMIDLOCKINGPROG	((uint32_t) 200000) /* Pyramid-locking	     */
#define RPC_PYRAMIDSYS5		((uint32_t) 200001) /* Pyramid-sys5	     */
#define RPC_CADDS_IMAGE		((uint32_t) 200002) /* CV cadds_image	     */

#define RPC_ADT_RFLOCKPROG	((uint32_t) 300001) /* ADT file locking	     */


#ifdef NFSSERVER
/*
 * Classification of NFS procedures.
 */
struct nfs_proc {
	int		nfs_proctype;
	xdrproc_t	nfs_xdrargs;
	int		nfs_argsz;
};

/*
 * NFS procedure argument structures.
 */
union nfs_rfsargs {
	fhandle_t fhandle;
	struct nfssaargs nfssaargs;
	struct nfsdiropargs nfsdiropargs;
	struct nfsreadargs nfsreadargs;
	struct nfswriteargs nfswriteargs;
	struct nfscreatargs nfscreatargs;
	struct nfsrnmargs nfsrnmargs;
	struct nfslinkargs nfslinkargs;
	struct nfsslargs nfsslargs;
	struct nfsrddirargs nfsrddirargs;
};

union nfs3_rfsargs {
	nfs_fh3 nfs_fh3_a;
	diropargs3 diropargs3_a;
	RENAME3args RENAME3args_a;
	LINK3args LINK3args_a;
};

#if (defined(SUNOS5) && !defined(SUNOS58)) || defined(IRIX6)
struct nfs2_timeval {
	uint32_t tv_sec;
	uint32_t tv_usec;
};
#endif

/*
 * Macros for use with RPC stuff.
 */
#define min(a, b)		((a) < (b) ? (a) : (b))
#define rpc_buffer(xprt)	((xprt)->xp_p1)
#define su_data(xprt)		((struct svcudp_data *)((xprt)->xp_p2))

/*
 * UDP service data.
 */
struct svcudp_data {
	u_int	su_iosz;	/* byte size of send/recv buffer	*/
	u_int	su_xid;		/* transaction id			*/
	XDR	su_xdrs;	/* XDR handle				*/
	char	su_verfbody[MAX_AUTH_BYTES];	/* verifier body	*/
};
#endif
bool_t xdr_creatargs(XDR *, struct nfscreatargs *);
bool_t xdr_diropargs(XDR *, struct nfsdiropargs *);
bool_t xdr_fhandle(XDR *, fhandle_t *);
bool_t xdr_linkargs(XDR *, struct nfslinkargs *);
bool_t xdr_rddirargs(XDR *, struct nfsrddirargs *);
bool_t xdr_readargs(XDR *, struct nfsreadargs *);
bool_t xdr_rnmargs(XDR *, struct nfsrnmargs *);
bool_t xdr_saargs(XDR *, struct nfssaargs *);
bool_t xdr_sattr(XDR *, struct nfssattr *);
bool_t xdr_slargs(XDR *, struct nfsslargs *);
bool_t xdr_nfs2_timeval(XDR *, struct nfs2_timeval *);
bool_t xdr_writeargs(XDR *, struct nfswriteargs *);

/* NFS3 stuff.  */
bool_t xdr_nfs_fh3(XDR *, nfs_fh3 *);
bool_t xdr_diropargs3(XDR *, diropargs3 *);
bool_t xdr_RENAME3args(XDR *, RENAME3args *);
bool_t xdr_LINK3args(XDR *, LINK3args *);
