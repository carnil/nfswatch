/*
 * $Id$
 *
 * HISTORY
 *
 */
/*
 * @(#)$RCSfile$ $Revision$ (DEC) $Date$
 */

/*
 * Cloned from ultrix.map.h, although this could really be a single
 * file.
 */

#ifndef ETHERTYPE_REVARP
/* some systems don't define this */
#define ETHERTYPE_REVARP	0x8035
#define REVARP_REQUEST		3
#define REVARP_REPLY		4
#endif  /* ETHERTYPE_REVARP */

/* Map protocol types */
#define ETHERPUP_IPTYPE ETHERTYPE_IP
#define ETHERPUP_REVARPTYPE ETHERTYPE_REVARP
#define ETHERPUP_ARPTYPE ETHERTYPE_ARP

/* newish RIP commands */
#ifndef	RIPCMD_POLL
#define	RIPCMD_POLL	5
#endif	/* RIPCMD_POLL */
#ifndef	RIPCMD_POLLENTRY
#define	RIPCMD_POLLENTRY	6
#endif	/* RIPCMD_POLLENTRY */
